using import ..init

fn print-products (a b)
    print "multiply" a b
    print "geom"
        * a b
    print "outer"
        outer a b
    print "left"
        left a b
    print "right"
        right a b
    print "dot"
        dot a b
    print "inner"
        inner a b
    print;

print-products
    scalof f32 -1
    vecof 3 f32 1 2 3

print-products
    vecof 3 f32 1 2 3
    vecof 3 f32 4 5 6

print-products
    vecof 3 f32 1 2 3
    bivecof 3 f32 4 5 6

print-products
    bivecof 3 f32 1 2 3
    bivecof 3 f32 4 5 6

print-products
    2
        vecof 3 f32 1 2 3

print-products
    2
        bivecof 3 f32 1 2 3

print-products
    2
        mvecof f32 (e1 = 1) (e2 = 2) (e12 = 12)

# TODO: * geom wrong?
        * add norm
