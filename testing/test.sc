if main-module?
    using import testing
    test-modules
        .test
        .complex
        .products
    exit 0

using import ..init
using import ..core
using import utils.va

#let vec =
    vecof f32 1 2 3
let vec0 =
    vecof 3 f32
        e1 = 1
let bivec =
    bivecof 3 f32 1 2 3
let rotor =
    as
        rotorof 3 f32 0 1 2 3
        versor

#'rotate rotor 

#let I =
    (e1 f32) * (e2 f32)

#print I (e12 f32)

#print "as" ((e1 f32 2.0) as (mvec f32 'e1 'e2))

let vec3 =
    mvec u32 'e1 'e2 'e3
print vec3
let a =
    vec3
        e1 = 0
        e2 = 1
        e3 = 2


print a

let scalar =
    make-mvec;

let vec1 vec2 vec3 vec4 =
    make-mvec (va-map basis-blade 1)
    make-mvec (va-map basis-blade 1 2)
    make-mvec (va-map basis-blade 1 2 3)
    make-mvec (va-map basis-blade 1 2 3 4)

let biv2 biv3 biv4 =
    make-mvec (basis-blade 1 2)
    make-mvec (basis-blade 1 2) (basis-blade 1 3) (basis-blade 2 3)
    make-mvec (basis-blade 1 2) (basis-blade 1 3) (basis-blade 1 4)
        \ (basis-blade 2 3) (basis-blade 2 4) (basis-blade 3 4)

let p =
    vec2 f32
        e1 = 1
        e2 = 2

let vec2* =
    make-mvec 'e23

let p2 =
    vec2* f32
        e2 = 2
        e3 = 3

#let p3 =
    p * p2 ####
                 (e1 + 2*e2) (2*e2 + 3*e3) =
                 2*e1-2 + 3*e1-3 + 4 + 6*e2-3

#compiler-error! "end"

#print "p3:" p3

let p3-outer p3-inner =
    outer p p2            # only the bivector part of previous
    inner p p2            # only the scalar part of previous

print p3-outer p3-inner

#print
    mvec* f32 1

let test =
    biv4 f32 1 2 3 4 5 6

let quat =
    make-mvec 'e
        basis-blade 1 2
        basis-blade 2 3
        basis-blade 3 1 # (basis-blade 1 3) and (basis-blade 3 1) are the same as 'e13

let angle axis = (pi / 4.0) (normalize (vectorof f32 0 1 1))
let sin* cos* =
    sin angle
    cos angle
let q =
    quat f32 sin* (unpack (axis * (vectorof f32 (3 cos*))))


print
    'conj q

print
    q * (vec3 f32 1 2 3)

let qq =
    bitcast q (versor f32 'e 'e12 'e13 'e23)

print "rotated"
    'rotate qq (vec3 f32 1 2 3)
    'rotate qq (vec3 f32 1 0 0)



#let q2 =
    *
        vec3 f32 1 1 0
        vec3 f32 1 0 1

let imag = (make-mvec 'e12)

let i = (imag f32 1)

print "i:" i

print "i*i:" (i * i)

print "conj:"
    'conj i
    'conj (i * i)

assert (== q.e13 (q @ 2) (q . e13))

let super-mvec =
    mvecof i32
        e = 12
        e12 = 3
        e123 = 2
        e4 = -1

print "super!" super-mvec
        

print "quat:" q

print "vecs:"
    vec3 f32 1 1 0
    (make-mvec (va-map basis-blade 1 2 3)) f32 1 0 1

#print p
#print test

let a b c d e f g =
    p + p
    p - p
    p * p
    2.0 * p
    p * 2.0
    inner p p
    outer p p

#let c =
    test + test

print "ops" a b c d e f g

let s-1 s0 s1 s2 =
    scalof f32 -1
    scalof f32 0
    scalof f32 1
    scalof f32 2

print "grade-m"
    grade-mvec f32 2 1

let v0 v1 =
    vecof f32 1 0
    vecof f32 0 1

let v v-o =
    v0 * v1
    outer v0 v1

print "v" v0 v v-o

print "exps"
    va-map 'exp
        \ s-1 s0 s1 s2
print "expv"
    va-map 'exp
        \ v0 v v-o

fn rotor-from-axis-angle (axis angle)
    let T = (typeof axis)
    let et = T.ElementType
    assert (et == (typeof angle))
    let pscal =
        pscalof 3 et (angle / (2 as et))
    let bivec =
        axis * pscal
    print "pab" pscal axis bivec
    let rotor =
        'exp bivec
    rotor

let axis =
    vecof f32 0 0 1

let angle1 angle2 =
    pi / 2.0
    pi / 3.0

let rotor1 rotor2 =
    rotor-from-axis-angle axis angle1
    rotor-from-axis-angle axis angle2

let vec =
    unconst
        vecof f32 1 0 1

#dump
    typify mvec.exp (mvec f32 (basis-blade 1 2) (basis-blade 1 3) (basis-blade 2 3))

print "v"
    vecof i8 1.0 2.0

print "rotors:" rotor1 rotor2

print "rotated"
    'rotate rotor1 vec # 0 1 1
    'rotate rotor2 vec # 0.5 ? 1

let bivec4 =
    bivecof 4 f32 1 2 3 4 5 6

#print
    bivec4 * bivec4

