using import .core
using import .common

#let scal scalof

let real = scal

fn realof (val)
    let T =
        typeof val
    (real T) val

fn imag (T)
    mvec T 3

fn imagof (val)
    let T =
        typeof val
    (imag T) val

fn complex (T)
    mvec T 0 3

fn complexof (T real imag)
    (complex T) real imag

#let I =
    imagof bool true

locals;
