using import utils.va
using import utils.utils
using import utils.argless
using import .core

let scope = (Scope)

fn make-e (blade)
    fn (type val)
        if (type? type)
            let val =
                some val 1
            (mvec type blade) val
        elseif (none? val)
            (mvec (typeof type) blade) type
        else
            error! "Invalid arguments to e initializer"

let loop0 (blade0) = 0
if (blade0 < (2 ** 5))
    let loop (blade1) = 0
    if (blade1 < (2 ** 4))
        let blade =
            blade0 * (2 ** 4) + blade1
        let blade-name = 
            .. (basis-blade-name blade) (if (and (blade == 0) (blade0 == 0)) "!")
        let symbol =
            Symbol blade-name
        
        set-scope-symbol! scope
            symbol
            make-e blade
        set-scope-docstring! scope
            symbol
            .. "Generates the basis blade " blade-name " by specifying a type and a value\n"
                "If the value is unspecified, the unit type of this blade will be created (the value will be one)\n"
                "If only a value, but no type, is specified, the type will be inferred"
        loop (blade1 + 1)
    loop0 (blade0 + 1)
scope

