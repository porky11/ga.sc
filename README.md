This aims to be a very efficient library for geometric algebra.

Depends on [my utils](https://gitlab.com/porky11/utils.sc)

Move or symlink the utils repo to `/path/to/lib/scopes/utils` in order to import it correctly.

## Usage

The main geometric algebra type is the multivector. It's a generalization of a vector.
The abstract multivector type is called `mvec`.
You can create specific multivector types by this:
`mvec type blades...`
This will create a multivector type, whose elements are saved as `type`.
The specified `blades...` can each be generated using the `blade` function, which takes integer values for the dimensions.
`blade 1` and `blade 2` both return a blade, which represent one dimensional orthogonal objects (basis vectors).

For example in order to create a 3D vector type, you can just use this:
```
mvec (blade 1) (blade 2) (blade 3)
```

If you pass multiple arguments to `blade`, they will generate a more dimensional blade.
A simple bivector could be represented by `blade 1 2` (or `blade 2 1`, which is identical)

If `blade` is called without arguments, it will represent a scalar value.

Instead, you can just specify the blades by a symbol starting with "e" and followed by the used dimensions.

A simple 3D vector type can also be written this way:
```
mvec 'e1 'e2 'e3
```

If you want to specify a multidimensional blade this way, the order of the digits has to be ascending.
Only single digit numbers are currently allowed when creating multivector types this way.

The generated types will be subtypes of `mvec`.

You can create the mvec by calling it with the values as arguments.
The order of the arguments is by ascending grade, and the grades sorted by the basis vector numbers.
So when you have the type `mvec 'e 'e1 'e2 'e12 'e3 'e13 'e23 'e123`, and call it `my-mvec`, then you initialize it in this order: `my-mvec e e1 e2 e3 e12 e13 e23 e123`.
You can also initialize it, using keyword-value-style.
The keys just are the blade names starting with `e`.
In this case, it's also possible to specify blades with multi-number-indices.
Then you have to write a `-` after the `e` and as seperator betwen the numbers.
So the blade representing the vector of index 10 is called `e-10` and some bivector, containitn `0` and `10` are called `e-0-10`
When using keyword-value-style, the unspecified values will be implicitely set to zero.

An easier way to create multi vector types is `mvecof`.
It takes a `type` argument and keyword-value-style arguments (only single digit `e` is supported).
The resulting type will contain all blades specified by the keys, but already return a value of this type.
For quick testing, this may be the most intuitive way, but there are also other functions, that you may want to try.

There's an abstract subtype of `mvec` called `versor`.
Versors are all types, that can be generated as a product of vectors.
When calling `mvec`, versors will be created implicitely, when all elements of this types are versors.
Currently this only happens for single blade multivectors (just one blade as argument) and vectors themselves (all blades have grade one).

If you explicitely want to create versors, just use `versor` as constructor instead of `mvec`.
Only do this, if you are sure, that your type is a versor. You should prefer `mvec`.

Casting of multivectors is possible using `as`. Casting `mvec` types to `versor` is not possible. This is probably a mistake. Instead, just cast to the wanted type as `mvec` and then just cast it to `versor` (the type itself, no arguments).
Casting to `versor` will just perform a bitcast to the versor type of the same dimension.

Addition and subtraction work for equal multivector types. Even if both types are versors, addition and subtraction may generate `mvec` types, when not all elements of this types are versors.

Multiplication is possible for arbitrary multivector types. The resulting type depends on the argument types. So the resulting type will have exactly the needed fields for the blades and no storage will be wasted.
Multiplication of two versors returns a new versor each time. Multiplication of a versor or multivector and another multivector just returns a multivector.

There are also special products, called `outer` and `inner` to represent the outer and inner product of multivectors.

Division only works for versors, not for other multivectors. If you accidently converted a multivector to a versor, this may become a problem, if you use this.


