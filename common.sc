using import .core


fn scal (type)
    mvec type 0
fn scalof (type val)
    """"Generates a scalar value for any dimension of `type` initialized by `val`
    (scal type) val

fn vec (dim type)
    grade-mvec type dim 1
fn vecof (type ...)
    """"Genrates a vector of `type` initialized using varargs or keyword arguments.
        If an integer is the first argument, instead of type, the dimension is explicitely set.
    fn real-vecof (dim type ...)
        (vec dim type) ...
    if ((typeof type) < integer)
        real-vecof type ...
    else
        real-vecof (va-countof ...) type ...

fn bivec (dim type)
    grade-mvec type dim 2
fn bivecof (dim type ...) =
    """"Generates a bivector for dimension `dim` of `type` initialized using varargs or keyword arguments.
    (bivec dim type) ...

fn rotor (dim type)
    grade-mvec type dim 0 2
fn rotorof (dim type ...) =
    """"Generates a rotor for dimension `dim` of `type` initialized using varargs or keyword arguments.
    (rotor dim type) ...


fn pscal (dim type)
    grade-mvec type dim dim
fn pscalof (dim type val)
    """"Generates a pseudoscalar for dimension `dim` of `type` initialized by `val`
    (pscal dim type) val


locals;

